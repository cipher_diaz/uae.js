// Some helper functions taken from index.js
// TODO: wrap them


function floppyEject(n) {
    var cfg = sae.getConfig();
    cfg.floppy.drive[n].file.clr();
    sae.eject(n);
}

function floppyInsert(n,data,name) {
	console.error("insert " + name);
    var cfg = sae.getConfig();
    var file = cfg.floppy.drive[n].file;
    file.name = name;
    file.data = data;
    file.size = data.length;
    file.crc32 = crc32(data);
    sae.insert(n);
    
}

function floppyLoad(n,url,reset) {
	getBinaryFileAsString(url,function(data){
		if (data){
			var name = url.split("/").pop();
			floppyInsert(n,data,name);
			if (reset) sae.reset()
		}
	});
}


var getDrivetimer;
function getDriveDelayed(){
	clearTimeout(getDrivetimer);
	getDrivetimer = setTimeout(function(){
		getDrive();
	},50);
}

function getDrive(){
	var disk = BinaryStream(SAER_state.floppy[0].diskfile.data.buffer,true);
	adf.setDisk(disk);
	listDisk();
}

function putDrive(){
	
	// TODO: maybe skip the "insert new disk" flow and write to the various internal buffers directly?
	// note: SAER_state.floppy[0].diskfile.data is not an arraybuffer but an Uint8Array
	// note2: only updating SAER_state.floppy[0].diskfile.data is not enough -> investigate;
	
	var disk = adf.getDisk();
	var data = arrayBufferToString(disk.buffer);
	
	var cfg = sae.getConfig();
	var file = cfg.floppy.drive[0].file;
	file.data = data;
	file.crc32 = crc32(data);
	sae.insert(0);
	
}

function writeAutoRun(command){
	var fileName = "Auto-StartUp";

	// find S folder

	var folder=adf.readRootFolder();
    var s = folder.folders.find(function(item){
		return item.name.toLowerCase() === "s"
    });

    if (s){
    	// delete previous if exists
        var folder=adf.readFolderAtSector(s.sector);
        var file = folder.files.find(function(item){
            return item.name === fileName
        });

        if (file) adf.deleteFileAtSector(file.sector);

        // write new file
        var buffer = new ArrayBuffer(command.length);
        var view = new Uint8Array(buffer);
        for (var i = 0, len = command.length; i < len; i++) {
            view[i] = command.charCodeAt(i);
        }

        adf.writeFile(fileName,buffer,s.sector);

	}

}


var fingerprint = [];
var diskhash = [];

function check(){

	var disk = BinaryStream(SAER_state.floppy[0].diskfile.data.buffer,true);
	adf.setDisk(disk);
	
	
	fingerprint.push(adf.getSectorMd5());
	diskhash.push({
		mfm: md5(SAER_state.floppy[0].bigmfmbuf),
		diskfile: md5(SAER_state.floppy[0].diskfile.data),
		newfile: md5(SAER_state.floppy[0].newfile.data),
	});
	
	if (fingerprint.length>1){
		var set1 = fingerprint[fingerprint.length-2];
		var set2= fingerprint[fingerprint.length-1];
		
		var changed = [];
		
		for (var i = 0, len = set1.length; i<len; i++){
			if (set1[i] !== set2[i]) changed.push(i);
		}
		
		set1 = diskhash[fingerprint.length-2];
		set2 = diskhash[fingerprint.length-1];
		
		var changedDisk = [];
		if (set1.mfm !== set2.mfm) changedDisk.push("mfm"); 
		if (set1.diskfile !== set2.diskfile) changedDisk.push("diskfile"); 
		if (set1.newfile !== set2.mfm) changedDisk.push("newfile"); 
		
		if (changed.length){
			console.error("disk changed: " + changedDisk.join(",") + ": " + changed.length + " sectors changed")
			console.log(changed);
		}else{
			console.log("disk not changed: " + changed.length + " sectors changed")
		}
		
	}
}

