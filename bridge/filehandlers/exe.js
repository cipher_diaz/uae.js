/*
	part of the Amiga filesystem implementation in javascript.

	MIT License

	Copyright (c) 2019 Steffest - dev@stef.be

	This is fake ... for demo purpose only

*/


var EXE = function(){

    var me = {};

    me.fileTypes={
        EXE: {
            name: "68k executable",
            actions:[{action: "run", label: "Run in UAE"}],
            inspect: true}
    };


    me.detect=function(file){
        var id = file.readWord(0);
        if (id === 0x0000){
            var id2 = file.readWord();
            if (id2 === 0x03F3){
                return FILETYPE.EXE;
            }
        }
    };

    me.inspect = function(file){
        var result = "";
        return result;
    };

    me.handle = function(file,action){
        if (action === "run"){
            writeAutoRun("DF0:programs/" + UI.getCurrentFile().name);
            putDrive();
        }
    };

    if (FileType) FileType.register(me);

    return me;
}();