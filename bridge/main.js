var sae;


function init() {
	
	UI.init();
	
    var model = SAEC_Model_A1200;
    var modelSubConfig = 0;

    sae = new ScriptedAmigaEmulator();

    var inf = sae.getInfo();
    //console.dir(inf);
	
    var cfg = sae.getConfig();

    sae.setModel(model, modelSubConfig);
    cfg.chipset.ntsc = false;
    cfg.memory.z2FastSize = 2<<20;
    cfg.floppy.speed = SAEC_Config_Floppy_Speed_Turbo;
	//cfg.floppy.speed = SAEC_Config_Floppy_Speed_Original;
	//cfg.floppy.autoEXT2 = 1;
   
    cfg.video.id = "suae";

    cfg.video.hresolution = SAEC_Config_Video_HResolution_HiRes;
    cfg.video.vresolution = SAEC_Config_Video_VResolution_Double;
    cfg.video.size_win.width = SAEC_Video_DEF_AMIGA_WIDTH << 1; /* 720 */
    cfg.video.size_win.height = SAEC_Video_DEF_AMIGA_HEIGHT << 1; /* 568 */

    cfg.video.xcenter = 2;
    cfg.video.ycenter = 2;

    cfg.video.antialias = false;
    cfg.video.cursor = SAEC_Config_Video_Cursor_Lock;

    //cfg.debug.level = SAEC_Config_Debug_Level_Log;
    cfg.debug.level = SAEC_Config_Debug_Level_Warn;


    cfg.hook.ide.read = function(sector,ide){
        //console.log("IDE read sector ",sector,ide);
    };

    cfg.hook.ide.write = function(sector,ide){
        //console.log("IDE write sector ",sector,ide);
    };


	cfg.hook.floppy.insert = function(drv){
		console.log("Floppy insert " + drv.num);
		if (drv.num === 0){
			getDriveDelayed();
		}
	};

	cfg.hook.floppy.read = function(drv){
		UI.blink("read");
		
	};

    cfg.hook.floppy.write = function(drv,writebuffer,drvsec){
		UI.blink("write");
        getDriveDelayed();
    };
    

    var romReady = false;
    var hdReady = false;

    var go = function(){
        if (romReady && hdReady){
            var err = sae.start();
            if (err !== SAEE_None) alert(saee2text(err));
        }
    };

    var boot13 = false;
    var bootfloppy = false;
    var rom =  boot13 ? "kick13.rom" : "kick31.rom";
    var hdName = "mini15.hdf";

	getBinaryFileAsString("data/" + rom,function(data){
        if (data){
            cfg.memory.rom.name = rom;
            cfg.memory.rom.data = data; /* typeof 'String' or 'Uint8Array' */
            cfg.memory.rom.size = data.length; /* size in bytes */
            cfg.memory.rom.crc32 = crc32(data); /* pre-calculate crc32 for a faster start */
            romReady = true;
            go();
        }
    });

    if (boot13){
		getBinaryFileAsString("data/Workbench13.adf",function(data){
            if (data){
                floppyInsert(0,data,"Workbench13.adf");
                hdReady = true;
                go();
            }
        });
    }else{
        
        if (bootfloppy){
			getBinaryFileAsString("data/BattleSquadron.adf",function(data){
				if (data){
					floppyInsert(0,data,"boot.adf");
					hdReady = true;
					go();
				}
			});
        }else{
			getBinaryFileAsString("data/" + hdName,function(data){
				if (data){
					var n = 0;
					var ci = cfg.mount.config[n].ci;
					SAER.setMountInfoDefaults(n);

					ci.controller_type = SAEC_Config_Mount_Controller_Type_MB_IDE;
					ci.controller_unit = n;
					//ci.controller_media_type = 0; // HD
					//ci.unit_feature_level = 1;
					ci.readonly = false;


					ci.file.name = hdName;
					ci.file.data = data;
					ci.file.size = data.length;
					ci.file.crc32 = false;

					console.error(data.length);

					var blocks = Math.floor(ci.file.size / ci.blocksize);
					ci.highcyl = Math.floor(blocks / (ci.surfaces * ci.sectors));
					ci.devname = "IDE" + n;


					hdReady = true;
					go();
				}
			});
        }
        
    }
    
}
