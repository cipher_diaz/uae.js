var UI = function(){
	var me = {};
	
	var diskLed;
	var overlay;
	var overlayPanel;
	var overlayContent;
	var overlayOkbutton;
	var boxContent;
	var diskwindow;
	var currentFile;
	var currentFolder = {sector:880};
	
	me.init=function(){
		diskLed = document.getElementById("diskled");
		overlay = document.getElementById("blanket");
        overlayPanel = document.getElementById("panel");
		overlayContent = document.getElementById("panelcontent");
        overlayOkbutton = document.getElementById("okbutton");
		boxContent = document.getElementById("boxcontent");
		diskwindow = document.getElementById("diskwindow");
	};


	me.blink = function(className){
		diskLed.classList.remove(className);
		setTimeout(function(){
			diskLed.classList.add(className);
		},10)
	};
	
	me.showOverlay = function(){
		overlay.style.display = "block";
	};

	me.hideOverlay = function(){
		overlayContent.innerHTML = "";
		overlay.style.display = "none";
	};

    me.resizeOverlay = function(width,height,hideButtons){
    	var totalHeight = height + 20 + 40 + 8;
        overlayPanel.className = "";
        if (width<150){
            overlayPanel.className = "small";
			totalHeight -= 40;
		}
		if (hideButtons){
			overlayPanel.className = "nobuttons";
			totalHeight -= 40;
		}
        overlayPanel.style.width = width + "px";
        overlayPanel.style.height = totalHeight + "px";
        overlayPanel.style.marginLeft = -Math.floor(width/2) + "px";
        overlayPanel.style.marginTop = -Math.floor(totalHeight/2) + "px";

        overlayContent.style.height = height + "px";


    };

    me.showFolder = function(f){
    	currentFolder = f;
		boxContent.className = "";
    	listDisk(currentFolder);
    };

    me.showFile = function(f){

        var container = document.getElementById("filecontent");
		var actions = document.getElementById("fileactions");
		actions.innerHTML = "";
		
		
        currentFile = f;
        var file = adf.readFileAtSector(f.sector,true);


        var content = "";
        content += '<h3>' + f.name + '</h3>';
        container.innerHTML = content;

        var fileType = FileType.detect(file.content);

        if (fileType){

            var info = fileType.name;
            if (fileType.info) {
            	if (fileType.info.text){
					info += "<br>" + fileType.info.text;
				}else{
					info += "<br>" + fileType.info;
				}
			}
            var intro = "This is a";
            if (["a","e","i","o","u"].indexOf(info.substr(0,1).toLowerCase())>=0) intro+="n";

            container.innerHTML += intro + " " + info;
            
            if (fileType.info && fileType.info.canvas){
				container.appendChild(fileType.info.canvas);
			}
            
            if (fileType.actions){
                fileType.actions.forEach(function(action){
                    var div = document.createElement("div");
                    div.innerHTML = action.label;
                    div.className = "action";
                    div.onclick = function(){fileType.handler.handle(fileType.file,action.action)};
                    actions.appendChild(div);
                });
            }
        }else{
			var btn = document.createElement("div");
			btn.onclick=function(){
				UI.textEdit(f);
			};
			btn.innerHTML = "Edit as Text";
			actions.appendChild(btn);
		}


		boxContent.className = "file";

		
        

    };

	me.textEdit = function(f){
        currentFile = f;
        console.error(currentFile);
		var file = adf.readFileAtSector(f.sector,true);
		var textarea = document.createElement("textarea");
		textarea.id = "textcontent";
		
		var content = "";

		for (var i = 0; i< file.size; i++){
			content += String.fromCharCode(file.content[i]);
		}
		
		textarea.value = content;

		overlayContent.innerHTML = "";
		overlayContent.appendChild(textarea);
        overlayOkbutton.style.display = "";

        UI.resizeOverlay(480,292);
		UI.showOverlay();
		
	};

    me.textEditSave = function(){
        var textarea = overlayContent.querySelector("textarea");
        if (textarea){
        	var content = textarea.value;
        	var buffer = new ArrayBuffer(content.length);
            var view = new Uint8Array(buffer);
            for (var i = 0, len = content.length; i < len; i++) {
                view[i] = content.charCodeAt(i);
            }

            adf.deleteFileAtSector(currentFile.sector);
            adf.writeFile(currentFile.name,buffer,currentFile.parent);
            listDisk();
            UI.hideOverlay();
            putDrive();
		}
	};

    me.showImage = function(canvas){
        UI.resizeOverlay(canvas.width,canvas.height);
        overlayContent.innerHTML = "";
        overlayContent.appendChild(canvas);
        overlayOkbutton.style.display = "none";
        UI.showOverlay();

    };

	me.showUrl = function(url){
		UI.resizeOverlay(800,600,true);
		overlayContent.innerHTML = "";
		var iframe = document.createElement("iframe");
		overlayContent.appendChild(iframe);
		iframe.src = url;
		overlayOkbutton.style.display = "none";
		UI.showOverlay();

	};

	

    me.upload = function(){
        var input = document.createElement('input');
        input.type = 'file';
        input.onchange = function(e){
            var files = e.target.files;
            if (files.length){
                var file = files[0];

                var reader = new FileReader();
                reader.onload = function(){
                    adf.writeFile(file.name,reader.result,currentFolder.sector);
                    listDisk();
                    putDrive();
                };
                reader.readAsArrayBuffer(file);
            }
        };
        input.click();
    };

    me.getCurrentFile = function(){
    	return currentFile;
	};
    
    me.setCurrentFolder = function(folder){
		currentFolder = folder;
	}

	return me;
}();


function listDisk(folder){
	var container = document.getElementById("diskcontent");
	var label = document.getElementById("disklabel");
	var diskwindow = document.getElementById("diskwindow");
	var actions = document.getElementById("diskactions");
	var boxContent = document.getElementById("boxcontent");
	

	
	var diskInfo = adf.getInfo();
	
	
	container.innerHTML = "";
	actions.innerHTML = "";
	boxContent.className = "";
	
	diskwindow.classList.add("active");
	
	if (diskInfo.diskFormat === "UNKNOWN"){
		
		label.innerHTML = ": NDOS: " + SAER_state.floppy[0].diskfile.name;

		var ndos = document.createElement("div");
		ndos.className = "ndos";
		ndos.innerHTML = "This is not a DOS disk";
		
		var tosec = document.createElement("div");
		tosec.className = "tosec";
		tosec.innerHTML = "Checking TOSEC ...";
		
		ndos.appendChild(tosec);
		container.appendChild(ndos);
		

		createButton("Boot in 3.1","bigbutton",container,function(){
			floppyLoad(0,'data/' + SAER_state.floppy[0].diskfile.name,true)
		});

		createButton("Boot in 1.3","bigbutton",container,function(){
			floppyLoad(0,'data/' + SAER_state.floppy[0].diskfile.name,true)
		});
		
		// check TOSEC
		checkTosec(adf.getMD5(),function(data){
			if (data && data.match > 0){
				tosec.innerHTML = "Disk identified in TOSEC as <span>" + data.result[0].name + "</span>";
			}else{
				tosec.innerHTML = "No exact TOSEC match"
			}
		});
		
		
	}else{

		label.innerHTML = ": " + diskInfo.label;
		
		var content;
		if (folder){
			content = adf.readFolderAtSector(folder.sector);
			
			var up = document.createElement("div");
			up.className = "folder";
			up.innerHTML = "..";
			up.onclick = function(){
				listDisk();
			};
			container.appendChild(up);


		}else{
			UI.setCurrentFolder({sector:880});
			content = adf.readRootFolder();
		}


		content.folders.forEach(function(item){
			var elm = document.createElement("div");
			elm.className = "folder";
			elm.innerHTML = item.name;
			elm.dataset.sector = item.sector;
			elm.onclick = function(){
				UI.showFolder(item);
			};
			container.appendChild(elm);
		});

		content.files.forEach(function(item){
			var elm = document.createElement("div");
			elm.className = "file";
			elm.innerHTML = item.name;
			elm.dataset.sector = item.sector;
			elm.onclick = function(){
				UI.showFile(item);
			};
			container.appendChild(elm);
		});

		
		createButton("Create Folder","",actions,function(){
			adf.createFolder("test",880);
			listDisk();
			putDrive();
		});

		createButton("Inject File","",actions,function(){
			UI.upload();
		});
	}
	
	
}

function createButton(label,className,container,onClick){
	var btn = document.createElement("div");
	btn.innerHTML = label;
	if (className) btn.className = className;
	btn.onclick=onClick;
	container.appendChild(btn);
	return btn;
}


function checkTosec(sMd5,next){
	var apiUrl = "https://www.stef.be/adfviewer/tosec/api/";

	var req = new XMLHttpRequest();

	req.onload = function(e) {
		var result = req.responseText;

		if (result && result.indexOf('"match"')){
			var data = JSON.parse(result);
			if (next) next(data);
		}else{
			if (next) next({match:0,error:"invalid response"});
		}
	};
	req.onerror = function(e){
		if (next) next({match:0,error:e});
	};
	req.open("GET", apiUrl + "?md5=" + sMd5);
	req.send();
};





