// Apparently SUAE expects binaries a String? weird. -> TODO investigate and change to arraybuffer
function getBinaryFileAsString(url,next){
	var req = new XMLHttpRequest();
	req.open("GET", url);
	req.overrideMimeType("text\/plain; charset=x-user-defined"); /* we want binary data */
	req.onload = function (event) {
		var content = req.responseText;
		if (content) {
			if (next) next(content);
		} else {
			console.error("unable to load", url);
			if (next) next(false);
		}
	};
	req.send(null);
}


// horribly inefficient ... We should really get rid of those string representations in SUAE
var arrayBufferToString;
if (typeof TextDecoder === "function"){
	arrayBufferToString = function(buffer){
		return new TextDecoder("x-user-defined").decode(buffer);
	}
}else{
	arrayBufferToString = function(buffer){
		var byteArray = new Uint8Array(buffer);
		var result = '';
		for (var i = 0; i < byteArray.byteLength; i++) {
			result += String.fromCodePoint(byteArray[i]);
		}
	}
}


const crc32Table = (function() {
	var table = new Uint32Array(256);
	var n, c, k;

	for (n = 0; n < 256; n++) {
		c = n;
		for (k = 0; k < 8; k++)
			c = ((c >>> 1) ^ (c & 1 ? 0xedb88320 : 0)) >>> 0;
		table[n] = c;
	}
	return table;
})();

function crc32(data) {
	var length = data.length;
	var offset = 0;
	var crc = 0xffffffff;

	while (length-- > 0)
		crc = crc32Table[(crc ^ data.charCodeAt(offset++)) & 0xff] ^ (crc >>> 8);

	return (crc ^ 0xffffffff) >>> 0;
}

function loadScript(url,next){
	var s = document.createElement('script');
	s.type = 'application/javascript';
	s.src = url;
	s.addEventListener('error', function(){
		console.error("Failed loading script " + url);
	}, false);
	s.addEventListener('load', function(){
		if (next) next();
	}, false);
	document.getElementsByTagName('head')[0].appendChild(s);
}
